---
layout: 2023/post
section: proposals
category: talks
author: Joan de Gracia, Francesc Busquets
title: Herramientas desarrolladas para la maqueta Linkat del Plan de Educación Digital de Catalunya
---

# Herramientas desarrolladas para la maqueta Linkat del Plan de Educación  Digital de Catalunya

>En esta ponencia se explicará de forma breve las diferentes herramientas desarrolladas para la maqueta Linkat de los ordenadores del Plan de Educación  Digital de Catalunya.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta / Presencial
-   Idioma: Español

-   Descripción:

>Para poder dar respuesta a las diferentes necesidades de los ordenadores Linkat del Plan de Educación Digital, se han desarrollado un conjunto de herramientas específicas para la maqueta Linkat. Se explicará como está construida la maqueta y el funcionamiento, entre otras cosas, de la herramienta de bloqueo remoto de ordenadores en caso de robo o desaparición, el sistema antivirus que analiza en tiempo real los ficheros del alumno/a, la herramienta de reasignación de equipos, el actualizador en línea del sistema, etc.

-   Web del proyecto: <http://linkat.xtec.cat>

-   Público objetivo:

>Personal técnico que participe en el desarrollo de una distribución GNU/Linux educativa.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: viernes 12 mayo 13:00-13:15
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Joan de Gracia, Francesc Busquets

-   Bio:

>Técnico docente del área de cultura digital del Departament d'Educació de la Generalitat de Catalunya. Actualmente codirijo el proyecto de distribución educativa Linkat GNU/Linux.

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
