---
layout: 2023/post
section: proposals
category: talks
author: Ignacio Sancho Morte
title: Vitalinux, migasfree y algunos esbirros... gestinando una distro
---

# Vitalinux, migasfree y algunos esbirros... gestinando una distro

>Vitalinux es la distribución de software libre basada en Ubuntu para educación de Aragón, pero no sería posible sin Migasfree y unos compañeros de trabajo. Descubre como ésta combinación permite una personalización y gestión a otro nivel y de forma casi desatendida

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>Desde el principio pensamos que Vitalinux debía ser una distribución que no se quedara en café para todos, sino que fuera personalizada y dinámica para cada centro. Migasfree nos permite hacer esto y muchas otras tareas como la instalación de drivers, gestión de software, gestión de sistemas auditoría y registro....y empleando solo a dos personas que mantienen de momento, un estado de salud mental aceptable.  Además contamos con la ayuda de algunos servidores esbirros en los centros que nos ayudan en nuestra tarea. ¿Cómo es ésto posible?

-   Web del proyecto: <http://wiki.vitalinux.educa.aragon.es>

-   Público objetivo:

>A cualquier organización que desee controlar de forma masiva, remota y desatendida sus sistemas informáticos.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: viernes 12 mayo 11:00-11:30
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Ignacio Sancho Morte

-   Bio:

>Docente de la Especialidad de Informática en Ciclos Formativos, actualmente como Asesor Técnico en Catedu  desarrollando, manteniendo y gestionando Vitalinux. Prefiero lo relacionado con los Sistemas, Redes y la Seguridad, y lloro un poco cuando tengo que ponerme a desarrollar.

### Info personal:

-   GitLab (u otra forja) o portfolio general: <https://gitlab.vitalinux.educa.aragon.es>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
