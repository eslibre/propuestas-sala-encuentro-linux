---
layout: 2023/post
section: proposals
category: talks
author: Miguel Ángel Rodríguez Muíños
title: EpiLinux&#58; una distro orientada a la epidemiología y la bioestadística
---

# EpiLinux: una distro orientada a la epidemiología y la bioestadística

>EpiLinux 6 es una distribución, basada en Ubuntu Budgie, orientada a la utilización de herramientas de análisis de datos epidemiológicos, bioestadísticos y de salud en general.<br><br>
Nace a raíz de la necesidad de facilitarle la migración a los usuarios de este tipo de estudios (perfiles sanitarios, en su mayor parte) desde aplicaciones privativas hacia otras libres y/o de código abierto y de aliviarles el proceso de selección y/o instalación de las mismas.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta / Presencial
-   Idioma: Español

-   Descripción:

>El objetivo de esta charla es la de dar a conocer el Proyecto EpiLinux (nacido en el año 2007) como distro, inicialmente, para el análisis de datos epidemiológicos (de ahí su nombre).<br><br>
Además de esto, se analizará el resultado obtenido a través de estos 16 años y del futuro del proyecto (EpiLinux 7? en qué condiciones?).<br><br>
Un objetivo colateral (pero no menos importante) es el de hacer networking con otros miembros de proyectos de distros estatales.

-   Web del proyecto: <https://www.sergas.es/Saude-publica/EpiLinux>

-   Público objetivo:

>El público objetivo son las personas que se dediquen al analisis de datos y/o a la formación en este campo. Además, es una charla destinada a los miembros de proyectos de distribuciones GNU/Linux con los que poner en comun distintos aspectos de la gestión de este tipo de proyectos.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: viernes 12 mayo 13:15-13:30
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Miguel Ángel Rodríguez Muíños

-   Bio:

>Técnico Informático de profesión. Caballero andante del Software Libre por vocación y músico amateur por afición. Miembro (y tesorero) de la Asociación de Usuarios de Software Libre MELISA (<https://www.melisa.gal/>).<br><br>
Bio abreviada:
- Coordinador de los proyectos EpiLinux (<https://www.sergas.es/Saude-publica/Epilinux>), BioStatFLOSS (<https://www.sergas.es/Saude-publica/BioStatFLOSS>) y AudioFLOSS (<https://audiofloss.melisa.gal/>)<br><br>
- Miembro del Comité Organizador y el Comité Científico de las "Xornadas de Usuarios de R en Galicia" (<https://www.r-users.gal/>)<br><br>
- Voluntario de la Asociación MELISA en actividades de formación y divulgación del software y la cultura libres

### Info personal:

-   Twitter: <https://twitter.com/@mianromu>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
