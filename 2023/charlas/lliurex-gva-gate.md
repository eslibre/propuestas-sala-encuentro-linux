---
layout: 2023/post
section: proposals
category: talks
author: Enrique Medina Gremaldos
title: LliureX GVA Gate
---

# LliureX GVA Gate

>GVA Gate es un conjunto de herramientas y servicios que hemos desarrollado en LliureX para poder hacer login con nuestro usuario del aula desde cualquier lugar.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>LliureX GVA Gate es una solución que permite hacer login desde cualquier equipo con LliureX 21 instalado. A diferencia de otras herramientas, este desarrollo propio nos permite funcionar sin una configuración previa, y manteniendo la estructura de grupos que necesitamos. En esta charla veremos los detalles de este proyecto y una pequeña prueba en directo.

-   Web del proyecto: <https://github.com/lliurex/lliurex-gva-gate>

-   Público objetivo:

>A cualquier entusiasta del software libre.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: viernes 12 mayo 12:30-13:00
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Enrique Medina Gremaldos
-   Bio:

>Mas de diez años de experiencia en el desarrollo de LliureX.

### Info personal:

-   Web personal: <https://github.com/Lt-henry>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
